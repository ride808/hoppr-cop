ARG BASE_IMAGE=docker.io/library/ubuntu
ARG BASE_TAG=23.10
ARG HOPPR_COP_HOME=/opt/hoppr-cop

# ----------------------------------------
# Hoppr-cop install stage
# ----------------------------------------
FROM $BASE_IMAGE:$BASE_TAG AS builder
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG APT_PKGS="curl python3 python3-apt python3-pip python3-venv ruby-full"
ARG CURL="curl --fail --silent --show-error --location --url"
ARG HOPPR_COP_HOME

# renovate: datasource=github-releases depName=anchore/grype/ versioning=semver
ARG GRYPE_VERSION=v0.56.0
ARG GRYPE_INSTALL=https://raw.githubusercontent.com/anchore/grype/${GRYPE_VERSION}/install.sh

# renovate: datasource=github-releases depName=aquasecurity/trivy/ versioning=semver
ARG TRIVY_VERSION=v0.45.1
ARG TRIVY_INSTALL=https://raw.githubusercontent.com/aquasecurity/trivy/${TRIVY_VERSION}/contrib/install.sh

COPY dist/hoppr_cop-*-py3-none-any.whl /tmp

# hadolint ignore=DL3008
RUN apt-get update \
  && apt-get install --yes --no-install-recommends ${APT_PKGS} \
  && apt-get clean \
  && rm -r /var/lib/apt/lists/* \
  && export PIP_TRUSTED_HOST="pypi.org pypi.python.org files.pythonhosted.org" \
  && python3 -m venv ${HOPPR_COP_HOME} \
  && ${HOPPR_COP_HOME}/bin/python3 -m pip install --no-cache-dir /tmp/hoppr_cop-*-py3-none-any.whl \
  && rm /tmp/hoppr_cop-*-py3-none-any.whl \
  && $CURL $GRYPE_INSTALL | sh -s -- -b /usr/local/bin $GRYPE_VERSION \
  && $CURL $TRIVY_INSTALL | sh -s -- -b /usr/local/bin $TRIVY_VERSION \
  && gem install semver_dialects \
  && apt-get autoremove --yes curl python3-pip python3-venv

# ----------------------------------------
# Final stage
# ----------------------------------------
FROM $BASE_IMAGE:$BASE_TAG

ARG HOPPR_COP_HOME

# Flatten build layers into single layer
COPY --from=builder / /

ENV XDG_CACHE_HOME=/cache
ENV CACHE_DIR=/cache
ENV PATH=${HOPPR_COP_HOME}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

VOLUME /cache
VOLUME /hoppr
WORKDIR /hoppr
ENTRYPOINT ["hoppr-cop"]
